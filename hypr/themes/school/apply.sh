#!/bin/bash
dir="$HOME/.config/hypr/themes/school"
# Wallpaper
#if [ -e "${HOME}/.cache/wal/colors" ]; then
#
#    wal -R --cols16
#
#else

pkill swaybg &
exec $(sleep 1 && swaybg -i $dir/wallpaper.png -m tile) &
 $(wal -i $dir/wallpaper.png) &
 $(wal -R) &
#fi

# Waybar
pkill waybar
 $(sleep 1 && waybar -c $dir/waybar/config.jsonc -s $dir/waybar/style.css)
