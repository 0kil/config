#!/bin/bash

## Autostart Programs

# Kill already running process
#_ps=()
#for _prs in "${_ps[@]}"; do
#	if [[ `pidof ${_prs}` ]]; then
#		killall -9 ${_prs}
#	fi
#done

# Audio
#exec-once = /usr/bin/pipewire & /usr/bin/pipewire-pulse & /usr/bin/wireplumber
exec pipewire & pipewire-pulse & sleep 5 & wireplumber



# swayidle
exec swayidle -w timeout 1200 'exec ~/.config/hypr/scripts/lock' timeout 1800 'doas zzz' &

# Polkit agent
#/usr/lib/xfce-polkit/xfce-polkit &

# Etc
#sleep 1
#/usr/libexec/xdg-desktop-portal-hyprland &
#sleep 2
#/usr/lib/xdg-desktop-portal &
#xdg-user-dirs-update
#fcitx5 &